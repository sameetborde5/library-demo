package com.library.impl.io.repository;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.library.api.service.filter.BookColumnsEnum;
import com.library.api.service.filter.BookFilter;
import com.library.api.service.filter.SortingEnum;
import com.library.impl.entity.BookEntity;
import com.library.impl.entity.BookEntity_;

public class BookRepositoryImpl implements BookRepositoryCustom {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	EntityManager entityManager;

	@Override
	public Optional<BookEntity> getBook(final Long bookId) {
		return this.bookRepository.findById(bookId);

	}

	@Override
	public BookEntity create(final BookEntity book) {
		return this.bookRepository.save(book);
	}

	@Override
	public void update(final BookEntity book) {
		this.bookRepository.save(book);

	}

	@Override
	public void delete(final Long bookId) {
		this.bookRepository.deleteById(bookId);
	}

	@Override
	public List<BookEntity> find(final BookFilter filter) {
		return getQuery(filter).getResultList();
	}

	private TypedQuery<BookEntity> getQuery(final BookFilter filter) {
		final CriteriaQuery<BookEntity> cr = createCriteriaBuilder(filter);
		final TypedQuery<BookEntity> query = this.entityManager.createQuery(cr);
		setPagingData(filter, query);
		return query;

	}

	private CriteriaQuery<BookEntity> createCriteriaBuilder(final BookFilter filter) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<BookEntity> criteriaQuery = criteriaBuilder.createQuery(BookEntity.class);

		final Root<BookEntity> bookEntity = criteriaQuery.from(BookEntity.class);

		criteriaQuery.where(findPredicates(criteriaBuilder, filter, bookEntity).toArray(new Predicate[] {}));

		setSortingData(filter, criteriaBuilder, criteriaQuery, bookEntity);

		return criteriaQuery;
	}

	private Set<Predicate> findPredicates(final CriteriaBuilder criteriaBuilder, final BookFilter filter,
			final Root<BookEntity> bookEntity) {

		final Set<Predicate> predicates = new HashSet<>();

		if (!StringUtils.isEmpty(filter.getAuthor())) {
			predicates.add(criteriaBuilder.like(bookEntity.get(BookEntity_.AUTHOR), "%" + filter.getAuthor() + "%"));
		}

		if (!StringUtils.isEmpty(filter.getPublisher())) {
			predicates.add(
					criteriaBuilder.like(bookEntity.get(BookEntity_.PUBLISHER), "%" + filter.getPublisher() + "%"));
		}

		if (!StringUtils.isEmpty(filter.getTitle())) {
			predicates.add(criteriaBuilder.like(bookEntity.get(BookEntity_.TITLE), "%" + filter.getTitle() + "%"));
		}

		if (filter.getPublishingDate() != null) {
			predicates.add(
					criteriaBuilder.equal(bookEntity.get(BookEntity_.PUBLISHING_DATE), filter.getPublishingDate()));
		}
		return predicates;
	}

	private void setSortingData(final BookFilter filter, final CriteriaBuilder criteriaBuilder,
			final CriteriaQuery<BookEntity> criteriaQuery, final Root<BookEntity> bookEntity) {

		final List<Order> orderList = new LinkedList<>();
		if (filter.getSortingMap().isEmpty()) {
			// Default sorting by Publication date in descending order
			orderList.add(getOrder(criteriaBuilder, bookEntity.get(BookEntity_.PUBLISHING_DATE), SortingEnum.DESC));
		} else {
			filter.getSortingMap().forEach((sortColumnKey, sortOrderValue) -> {
				if (sortColumnKey.equals(BookColumnsEnum.TITLE)) {
					orderList.add(getOrder(criteriaBuilder, bookEntity.get(BookEntity_.TITLE), sortOrderValue));
				} else if (sortColumnKey.equals(BookColumnsEnum.AUTHOR)) {
					orderList.add(getOrder(criteriaBuilder, bookEntity.get(BookEntity_.AUTHOR), sortOrderValue));
				} else if (sortColumnKey.equals(BookColumnsEnum.PUBLISHER)) {
					orderList.add(getOrder(criteriaBuilder, bookEntity.get(BookEntity_.PUBLISHER), sortOrderValue));
				} else if (sortColumnKey.equals(BookColumnsEnum.PUBLISHING_DATE)) {
					orderList.add(
							getOrder(criteriaBuilder, bookEntity.get(BookEntity_.PUBLISHING_DATE), sortOrderValue));
				}
			});
		}

		criteriaQuery.orderBy(orderList);
	}

	private Order getOrder(final CriteriaBuilder criteriaBuilder, final Path<Object> path,
			final SortingEnum sortingEnum) {
		if (sortingEnum.equals(SortingEnum.ASC)) {
			return criteriaBuilder.asc(path);
		} else {
			return criteriaBuilder.desc(path);
		}
	}

	private void setPagingData(BookFilter filter, final TypedQuery<BookEntity> query) {
		query.setFirstResult(filter.getPageNo());
		query.setMaxResults(filter.getPagesize());
	}

}
