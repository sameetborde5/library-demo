package com.library.impl.io.repository;

import java.util.List;
import java.util.Optional;

import com.library.api.service.filter.BookFilter;
import com.library.impl.entity.BookEntity;

public interface BookRepositoryCustom {

    Optional<BookEntity> getBook(Long bookId);

    BookEntity create(BookEntity book);

    void update(BookEntity book);

    void delete(Long bookId);

    List<BookEntity> find(BookFilter filter);
}
