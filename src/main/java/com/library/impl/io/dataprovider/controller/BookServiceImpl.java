/**
 *
 */
package com.library.impl.io.dataprovider.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.library.api.model.Book;
import com.library.api.service.BookService;
import com.library.api.service.filter.BookFilter;
import com.library.impl.io.repository.BookRepositoryCustom;
import com.library.impl.mapper.BookMapper;
import com.library.impl.usecase.BooksUsecase;

/**
 * Book resources controller
 *
 * @author s.borde
 *
 */
@RestController
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepositoryCustom bookRepository;

    @Autowired
    BookMapper mapper;

    @Override
    public List<Book> getBooksByFilter(final BookFilter filter) {
        return BooksUsecase.getAllBooks(this.bookRepository, this.mapper, filter);
    }

    @Override
    public Book getBook(@PathVariable(
        name = "id") final Long bookId) {

        return BooksUsecase.getBook(this.bookRepository, this.mapper, bookId);
    }

    @Override
    public void deleteBook(@PathVariable(
        name = "id") final Long bookId) {

        BooksUsecase.deleteBook(this.bookRepository, this.mapper, bookId);
    }

    @Override
    public Book createBook(@RequestBody final Book book) {

        return BooksUsecase.createBook(this.bookRepository, this.mapper, book);

    }

    @Override
    public void updateBook(@RequestBody final Book bookToUpdate, @PathVariable(
        name = "id") final Long bookId) {

        BooksUsecase.updateBook(this.bookRepository, this.mapper, bookToUpdate, bookId);
    }

}
