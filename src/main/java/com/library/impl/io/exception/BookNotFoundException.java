package com.library.impl.io.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author s.borde
 *
 */
@ResponseStatus(
    value = HttpStatus.NOT_FOUND)
public class BookNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 3358261089035897382L;

    private final String id;

    public BookNotFoundException(final String id) {
        super(String.format(" not found :'%s'", id));
        this.id = id;

    }

    public String getId() {
        return this.id;
    }

}
