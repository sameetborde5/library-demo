package com.library.impl.io.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author s.borde
 *
 */
@ResponseStatus(
    value = HttpStatus.BAD_REQUEST)
public class BookCreateException extends RuntimeException {

    private static final long serialVersionUID = -5444905904666804853L;

    public BookCreateException() {
        super("Bad request , create cannot have an id");
    }

}
