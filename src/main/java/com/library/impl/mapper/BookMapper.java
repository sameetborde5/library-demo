package com.library.impl.mapper;

import com.library.api.model.Book;
import com.library.impl.entity.BookEntity;

/**
 * @author s.borde
 *
 */
public interface BookMapper {

    Book convertToBusinessObject(BookEntity book);

    BookEntity convertToEntity(Book book);

}
