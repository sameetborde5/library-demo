/**
 *
 */
package com.library.impl.mapper.impl;

import org.springframework.stereotype.Component;

import com.library.api.model.Book;
import com.library.impl.entity.BookEntity;
import com.library.impl.mapper.BookMapper;

/**
 * @author s.borde
 *
 */
@Component
public class BookMapperImpl implements BookMapper {

    @Override
    public Book convertToBusinessObject(final BookEntity book) {

        return Book.builder()
            .id(book.getId())
            .author(book.getAuthor())
            .publisher(book.getPublisher())
            .title(book.getTitle())
            .publishingDate(book.getPublishingDate())
            .build();
    }

    @Override
    public BookEntity convertToEntity(final Book book) {
        return BookEntity.builder()
            .id(book.getId())
            .author(book.getAuthor())
            .publisher(book.getPublisher())
            .title(book.getTitle())
            .publishingDate(book.getPublishingDate())
            .build();
    }

}
