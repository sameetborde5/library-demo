/**
 *
 */
package com.library.impl.usecase;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.library.api.model.Book;
import com.library.api.service.filter.BookFilter;
import com.library.impl.entity.BookEntity;
import com.library.impl.io.exception.BookCreateException;
import com.library.impl.io.exception.BookNotFoundException;
import com.library.impl.io.repository.BookRepositoryCustom;
import com.library.impl.mapper.BookMapper;

/**
 * Usecase for book CRUD operations
 * 
 * @author s.borde
 *
 */
public class BooksUsecase {

	public static Book getBook(final BookRepositoryCustom bookRepository, final BookMapper mapper, final Long bookId) {
		return mapper.convertToBusinessObject(BooksUsecase.getBookFromRepository(bookRepository, mapper, bookId));
	}

	public static void deleteBook(final BookRepositoryCustom bookRepository, final BookMapper mapper,
			final Long bookId) {

		BooksUsecase.getBookFromRepository(bookRepository, mapper, bookId);

		bookRepository.delete(bookId);
	}

	public static Book createBook(final BookRepositoryCustom bookRepository, final BookMapper mapper, final Book book) {

		if (book.getId() != null) {
			throw new BookCreateException();
		}

		final BookEntity newBook = bookRepository.create(mapper.convertToEntity(book));

		return mapper.convertToBusinessObject(newBook);
	}

	public static void updateBook(final BookRepositoryCustom bookRepository, final BookMapper mapper, final Book book,
			final Long bookId) {

		BooksUsecase.getBookFromRepository(bookRepository, mapper, bookId);

		bookRepository.update(mapper.convertToEntity(book));
	}

	public static List<Book> getAllBooks(final BookRepositoryCustom bookRepository, final BookMapper mapper,
			final BookFilter filter) {

		return bookRepository.find(filter).stream().map(each -> mapper.convertToBusinessObject(each))
				.collect(Collectors.toList());
	}

	public static BookEntity getBookFromRepository(final BookRepositoryCustom bookRepository, final BookMapper mapper,
			final Long bookId) {
		final Optional<BookEntity> optional = bookRepository.getBook(bookId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new BookNotFoundException("id = " + bookId);
		}
	}

}
