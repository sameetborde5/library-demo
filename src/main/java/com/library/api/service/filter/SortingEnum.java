/**
 *
 */
package com.library.api.service.filter;

/**
 * @author s.borde
 *
 */
public enum SortingEnum {

    ASC,
    DESC;
}
