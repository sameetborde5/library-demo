/**
 * 
 */
package com.library.api.service.filter;

/**
 * @author s.borde
 *
 */
public enum BookColumnsEnum {
    
    TITLE,
    AUTHOR,
    PUBLISHER,
    PUBLISHING_DATE;

}
