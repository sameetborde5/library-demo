package com.library.api.service.filter;

import java.util.Date;
import java.util.Map;

import lombok.Builder;
import lombok.Data;

/**
 * @author s.borde
 *
 */
@Data
@Builder
public class BookFilter {
	private static final int DEFAULT_PAGE_SIZE = 20;

	private String title;
	private String author;
	private String publisher;
	private Date publishingDate;
	private Map<BookColumnsEnum, SortingEnum> sortingMap;
	private int pageNo;
	@Builder.Default
	private int pagesize = DEFAULT_PAGE_SIZE;

}
