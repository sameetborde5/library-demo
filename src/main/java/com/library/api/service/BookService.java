/**
 *
 */
package com.library.api.service;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.library.api.model.Book;
import com.library.api.service.filter.BookFilter;
import com.library.impl.io.exception.BookNotFoundException;

import javassist.NotFoundException;

@RequestMapping("/books")
public interface BookService {

    @PostMapping("/filter")
    public List<Book> getBooksByFilter(@RequestBody BookFilter filter);

    /**
     * Retrieves a book by the specified unique id if exists else throws BookNotFoundException
     *
     * @param bookId
     * @return
     * @throws NotFoundException
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Book getBook(@PathVariable(
        name = "id") Long bookId);

    /**
     * Deletes a book by the specified unique book id
     *
     * @param bookId
     * @return
     * @throws NotFoundException
     * @throws BookNotFoundException
     */
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteBook(@PathVariable(
        name = "id") Long bookId);

    /**
     * Creates a book and returns the saved book details.
     *
     * @param newBook
     */
    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Book createBook(@RequestBody Book newBook);

    /**
     * Updates the book details
     *
     * @param bookToUpdate
     * @param bookId
     * @throws NotFoundException
     * @throws BookNotFoundException
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateBook(@RequestBody Book bookToUpdate, @PathVariable(
        name = "id") Long bookId);

}
