insert into book(id, title, author, publisher, publishing_date) values (1001, 'Head First Java','Kathy Sierra','OREILLY', '2011-06-01' );
insert into book(id, title, author, publisher, publishing_date) values (1002, 'Java Programming','Joyce Farrell','Cengage Learning, Inc', '2012-06-01' );
insert into book(id, title, author, publisher, publishing_date) values (1003, 'Java The Complete Reference','SCHILDT','Jain Book Agency', '2013-06-01' );
insert into book(id, title, author, publisher, publishing_date) values (1004, 'Spring Book4','Author_4','Book4 Agency', '2014-06-01' );
insert into book(id, title, author, publisher, publishing_date) values (1005, 'Angular Book5','Author_5','Book5 Agency', '2015-06-01' );
insert into book(id, title, author, publisher, publishing_date) values (1006, 'Spring Boot Book6','Author_6','Book6 Agency', '2016-06-01' );
insert into book(id, title, author, publisher, publishing_date) values (1007, 'WildFly Book7','Author_7','Book7 Agency', '2017-06-01' );