package com.library.api.service;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.library.api.model.Book;
import com.library.impl.entity.BookEntity;
import com.library.impl.io.repository.BookRepositoryCustom;
import com.library.impl.mapper.BookMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookService.class, secure = false)
public class BookServiceTest {


	@MockBean
	private BookRepositoryCustom bookRepository;

	@MockBean
	private BookMapper bookMapper;

	@Test
	public void createBook() throws Exception {
		Book mockBook = new Book(1004l, "Sam_Title1", "Sam_Author1", "Sam_Publication",
				Calendar.getInstance().getTime());
		Mockito.when(bookRepository.create(Mockito.any(BookEntity.class))).thenReturn(new BookEntity(1004l,
				"Sam_Title1", "Sam_Author1", "Sam_Publication", Calendar.getInstance().getTime()));
		Mockito.when(bookMapper.convertToBusinessObject(Mockito.any(BookEntity.class))).thenReturn(mockBook);

	}

}
